#!/bin/bash -xe
WorkingDir=$(pwd)
ParserDir="/home/ops/app-pack-generator/"
Parser="$WorkingDir/app_pack_generator/parser.py"
PythonExe="python3"
ActivateVEnv="source ./env/bin/activate"

TestingDir="/home/zhan/downsample-landsat"
ARTIFACT_DIR="$WorkingDir/artifact-deposit-repo"
# ARTIFACT_URL="git@gitlab.com:geospec/artifact-deposit-repo.git"
ARTIFACT_URL="https://gitlab.com/geospec/artifact-deposit-repo.git"

echo "$(ls -la)"
# echo "$(ls -la $ParserDir)"

GIT_SSH_COMMAND="ssh -i $ARTIFACT_SSH" git clone "$ARTIFACT_URL" "$ARTIFACT_DIR"

echo "$($PythonExe -m venv env)"
$ActivateVEnv
echo "$($PythonExe -m pip install -r requirements.txt)"
env ARTIFACT_DIR=$ARTIFACT_DIR $PythonExe "$Parser" "$repository" "$checkout"
deactivate

cd $ARTIFACT_DIR
GIT_SSH_COMMAND="ssh -i $ARTIFACT_SSH" git push || true
